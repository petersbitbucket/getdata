# 取鉅亨聊天訊息

## 使用：
- 安裝mongodb https://www.mongodb.com/try/download/community
- 安裝nodejs https://nodejs.org/zh-tw/download/ (64bit 安裝包)
- 下載程式碼於C:/，再將目錄資料夾改名為node
- cd C:/node
- npm install
- npm start (看code 能不能運行正常，順利看到===爬蟲結束===，可按CTRL + C 結束node)
- 排定排程
```
在"左方列表"選擇"電腦管理 -> 系統工具 -> 工作排程器，可以在"中間視窗"瀏覽相關的執行工作，包含各個工作名稱、當前狀態(就緒或執行中等等)、工作觸發條件等等。
"右方子視窗"則有"動作視窗"，可以在此建立基本工作。

建立基本排程工作時，啟動的程式選擇 active.bat檔，週期設定那些看需求，設好後也可再編輯調整。
```