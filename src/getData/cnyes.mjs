import puppeteer from 'puppeteer';
import cheerio from 'cheerio';
import moment from 'moment'
import Msg from '../models/Msg.mjs';

const cnyes = async() => {
    console.log('===爬蟲開始===')
    const browser = await puppeteer.launch();
    const page = await browser.newPage();
    try {
        await page.goto('https://stock.cnyes.com/market/TWS:TSE01:INDEX');
        const htmlDom = await page.content()
        const msgList = dataHandler(htmlDom);
        await dbHandler(msgList);
        console.log('===爬蟲結束===')
        process.exit()
    } catch (error) {
        process.exit()
    }
}

const dbHandler = async(msgList) => {
    const result = await Msg.find({}).sort({_id:-1}).limit(1);
    if(result.length === 0){
        try {
            const res = await Msg.insertMany(msgList);
            console.log(res)
        } catch (error) {
            console.log('error', error);
        }
    }else{
        for (let item of msgList) {
            const itemTime = moment(item.date + ' ' + item.time, 'YYYY/MM/DD HH:mm:ss');
            const lastDbTime = moment(result[0].date + ' ' + result[0].time, 'YYYY/MM/DD HH:mm:ss');
            if(itemTime.isAfter(lastDbTime)){
                const newMsg = new Msg(item);
                const saveMsg = await newMsg.save();
                console.log('insert', saveMsg);
            }
        }
    }
}

const dataHandler = (htmlDom) => {
    let $ = cheerio.load(htmlDom);
    const list = $('div').attr('data-tid', 'chat-room-list')

    const msgList = [];
    
    $('li').each((key, item) => {
        const msg = $(item).html();
        $ = cheerio.load(msg);
        if($('img').attr('src') != null){
            const msgObj = {};
            msgObj.img = $('img').attr('src');
            msgObj.name = $('span').eq(0).text();
            msgObj.message = $('div').eq(1).text();
            msgObj.time = $('span').eq(1).text();
            msgObj.date = moment().format('YYYY/MM/DD');
            msgList.push(msgObj);
        }
    })
    return msgList.reverse();
}

export default cnyes;