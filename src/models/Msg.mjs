// var mongoose = require('mongoose');
import mongoose from 'mongoose';
const Schema = mongoose.Schema;

const MsgSchema = new mongoose.Schema({
    img: {
        type: String,
    },
    name: {
        type: String,
    },
    message: {
        type: String
    },
    time: {
        type: String
    },
    date: {
        type: String
    }
}, { timestamps: true })


const Msg = mongoose.model('Msg',MsgSchema);
export default Msg;